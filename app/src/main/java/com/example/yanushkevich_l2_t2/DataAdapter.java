package com.example.yanushkevich_l2_t2;

import android.view.LayoutInflater;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;

class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
    private LayoutInflater inflater;
    private List<PersonInfo> persons;

    DataAdapter(Context context, List<PersonInfo> persons){
        this.persons = persons;
        this.inflater = LayoutInflater.from(context);
    }
    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.person_info, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DataAdapter.ViewHolder holder, int position){
        PersonInfo person = persons.get(position);
        holder.imageView.setImageResource(person.getImage());
        holder.ageView.setText(person.getAge());
        holder.nameView.setText(person.getName());
    }

    public int getItemCount(){
        return persons.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final ImageView imageView;
        final TextView ageView, nameView;
        ViewHolder(View view){
            super(view);
            imageView = (ImageView)view.findViewById(R.id.avatar_image);
            nameView = (TextView)view.findViewById(R.id.name_text);
            ageView = (TextView)view.findViewById(R.id.age_text);
        }
    }
}
