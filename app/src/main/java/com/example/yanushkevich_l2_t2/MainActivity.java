package com.example.yanushkevich_l2_t2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.Menu;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    List<PersonInfo> persons = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       setInitialData();

       RecyclerView recyclerView = (RecyclerView)findViewById(R.id.list_person);

       DataAdapter adapter = new DataAdapter(this, persons);

       recyclerView.setAdapter(adapter);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.humburger_icon);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    private void setInitialData(){
        int i = 0;
        while (i < 20){
            persons.add(new PersonInfo("Unkown", "24", R.drawable.face));
            i++;
        }
    }
}
