package com.example.yanushkevich_l2_t2;

public class PersonInfo {
    private String age;
    private String name;
    private int image;

    public PersonInfo(String name, String age, int image){
        this.name = name;
        this.age = age;
        this.image = image;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
